<?php

namespace Modules\Product\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class ListOfProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (range(1, 4) as $i) {
            Product::create([
                'name' => 'Product ' . $i,
                'price' => 10.5 * $i,
                'description' => 'Product ' . $i . ' description',
                'images' => json_encode([['img_name' => 'product-'.$i.'jpg', 'url' => '/assets/product/'.'product-'.$i.'jpg']]),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
