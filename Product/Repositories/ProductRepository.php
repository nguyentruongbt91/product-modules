<?php

namespace Modules\Product\Repositories;

use Modules\Product\Entities\Product;

class ProductRepository extends BaseRepository
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Product::class;
    }
}