<?php

namespace Modules\Product\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Product\Repositories\ProductRepository;
use Illuminate\Routing\Controller;

class IndexController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Api show all products
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit') ?? 20;

        return $this->productRepository->getAllWithPagination($limit);
    }
}
