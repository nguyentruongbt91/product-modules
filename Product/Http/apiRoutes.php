<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' =>'/products'], function (Router $router) {
    $router->get('/', [
        'as' => 'product.index',
        'uses' => 'IndexController@index'
    ]);
});